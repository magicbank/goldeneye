import glob, os
from PIL import Image
from random import randint

def jpeg(origin, name):
    (w, h) = origin.size
    print("dimension => w: {:.0f} h: {:.0f}".format(w, h))

    output = Image.new("RGB", (w, h))

    (a, b, c) = origin.getpixel((1,1))
    print("mask => r: {:.0f} g: {:.0f} b: {:.0f}".format(a, b, c))

    for i in range(w) :
        for j in range(h) :
            (w, x, y) = origin.getpixel((i, j))
            if w == a and x == b and y == c:
                w = randint(0,255)
                x = randint(0,255)
                y = randint(0,255)
            elif w == 0 and x == 0 and y == 0:
                w = randint(0,255)
                x = randint(0,255)
                y = randint(0,255)
            elif w == 255 and x == 255 and y == 255:
                w = randint(0,255)
                x = randint(0,255)
                y = randint(0,255)

            output.putpixel((i, j), (w, x, y))


    output.save("output/{}-mask.jpg".format(name), "JPEG")

def png(origin, name):
    (w, h) = origin.size
    print("dimension => w: {:.0f} h: {:.0f}".format(w, h))

    output = Image.new("RGB", (w, h))

    (a, b, c, d) = origin.getpixel((1,1))
    print("mask => r: {:.0f} g: {:.0f} b: {:.0f} a:{:.0f}".format(a, b, c, d))

    for i in range(w) :
        for j in range(h) :
            (w, x, y, z) = origin.getpixel((i, j))
            if w == a and x == b and y == c:
                w = randint(0,255)
                x = randint(0,255)
                y = randint(0,255)
            elif w == 0 and x == 0 and y == 0:
                w = randint(0,255)
                x = randint(0,255)
                y = randint(0,255)
            elif w == 255 and x == 255 and y == 255:
                w = randint(0,255)
                x = randint(0,255)
                y = randint(0,255)

            output.putpixel((i, j), (w, x, y, z))


    output.save("output/{}-mask.jpg".format(name), "JPEG")

if __name__ == '__main__' :
    for infile in glob.glob("*.jpg"):
        print("################################")
        print(infile)
        file = Image.open(infile)
        print("Format => {}".format(file.format))
        name, ext = os.path.splitext(infile)
        if file.format == "JPEG":
            jpeg(file, name)
        elif file.format == "PNG":
            png(file, name)
    for infile in glob.glob("*.jpeg"):
        print("################################")
        print(infile)
        file = Image.open(infile)
        print("Format => {}".format(file.format))
        name, ext = os.path.splitext(infile)
        if file.format == "JPEG":
            jpeg(file, name)
        elif file.format == "PNG":
            png(file, name)
    for infile in glob.glob("*.png"):
        print("################################")
        print(infile)
        file = Image.open(infile)
        print("Format => {}".format(file.format))
        name, ext = os.path.splitext(infile)
        if file.format == "JPEG":
            jpeg(file, name)
        elif file.format == "PNG":
            png(file, name)
