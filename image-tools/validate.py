import os
import sys
import glob
import getopt
import numpy
from PIL import Image

if __name__ == "__main__":
    i = ""
    o = ""

    options, _ = getopt.getopt(sys.argv[1:], "i:o:", ["input=", "output="])

    for opt, arg in options:
        if opt in ("-i", "--input"): i = arg
        if opt in ("-o", "--output"): o = arg

    if i == "" or o == "":
        print "usage: python validate.py -i <input> -o <output>"
        sys.exit(2)

    for f in glob.glob("{}/*.*".format(i)):
        p, n = os.path.split(f)
        name, x = os.path.splitext(n)

        if x == ".png" or x == ".jpg" or x == ".jpeg":
            img = Image.open(f)
            if img.format == "PNG":
                print "Processing: {} Format: {} Mode: {}".format(f, img.format, img.mode)
                img.load()
                if img.mode == "RGBA":
                    bg = Image.new("RGB", img.size, (255, 255, 255))
                    bg.paste(img, mask=img.split()[3])
                    bg.save("{}/{}-convert.jpg".format(o, name), "JPEG", quality=80)
                else:
                    img.save("{}/{}-convert.jpg".format(o, name), "JPEG", quality=80)
                    

